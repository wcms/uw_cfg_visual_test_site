<?php

/**
 * @file
 * uw_cfg_visual_test_site.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_visual_test_site_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view style guides'.
  $permissions['view style guides'] = array(
    'name' => 'view style guides',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'styleguide',
  );

  return $permissions;
}
